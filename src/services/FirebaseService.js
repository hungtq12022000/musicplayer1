import { initializeApp } from "firebase/app";
import {
  getDatabase,
  ref,
  onChildAdded,
  set,
  push,
  remove,
  child,
} from "firebase/database";
import YoutubeServices from "./YoutubeServices";
import { YoutubeAction } from "../redux/actions";
import Store from "../redux/Store";

const firebaseConfig = {
  apiKey: "AIzaSyAE5Fgfy-ZKmXLxIhhVPyrre7wbRMfT_-E",
  authDomain: "musicplayer-e40b0.firebaseapp.com",
  databaseURL: "https://musicplayer-e40b0-default-rtdb.firebaseio.com",
  projectId: "musicplayer-e40b0",
  storageBucket: "musicplayer-e40b0.appspot.com",
  messagingSenderId: "328774120389",
  appId: "1:328774120389:web:b825b7c0979717ff0091ac",
  measurementId: "G-KDTJLZYEJN",
};
initializeApp(firebaseConfig);

class FirebaseService {
  constructor() {
    this.onNewSongAdded();
  }

  addNewSong = (data) => {
    const db = getDatabase();
    const songListRef = ref(db, "list");
    const newSongRef = push(songListRef);
    set(newSongRef, {
      ...data,
    });
    // thêm bài hát lên server
  };

  removeSong = (primaryKey) => {
    const db = getDatabase();
    remove(ref(db, "list" + `/${primaryKey}`));
  };

  onNewSongAdded = () => {
    const db = getDatabase();
    const songListRef = ref(db, "/list");
    onChildAdded(songListRef, (data) => {
      const primaryKey = data.ref._path.pieces_[1]
      Store.dispatch({
        type: YoutubeAction.UPDATE_LIST,
        data: { ...data.val(), primaryKey },
      });
      YoutubeServices.initPlayer();
    });
    // đưa bài vừa thêm vào database
  };
}

export default new FirebaseService();
