import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { YoutubePlayer, PlaylistItem, ButtonPrimary } from "../../components";
import { useNavFunc } from "../../navigation/useNavFunc";
import { YoutubeAction } from "../../redux/actions";
import FirebaseService from "../../services/FirebaseService";

const Home = () => {
  const { navigation } = useNavFunc();
  const { listMusic, playing } = useSelector((state) => state.youtube);
  const dispatch = useDispatch();

  const onEnd = () => {
    dispatch({
      type: YoutubeAction.NEXT_SONG,
    });
  };

  const playSong = (item) => {
    FirebaseService.removeSong(item.primaryKey);
    dispatch({
      type: YoutubeAction.PLAY_SONG,
      data: item,
    });
  };

  return (
    <View style={styles.container}>
      {Boolean(playing) && (
        <YoutubePlayer
          onEnd={onEnd}
          onReady={(event) => {
            event.target.playVideo();
          }}
          videoId={playing.videoId}
          nameSong={playing.title}
          nameAuthor={playing.author}
        />
      )}
      <View style={styles.wrapper}>
        <Text style={styles.playlistTitle}>PlayList</Text>
        {listMusic.map((item, id) => {
          return (
            <PlaylistItem
              key={`item${id}`}
              thumb={item.thumbnail}
              name={item.title}
              author={item.author}
              onPress={() => playSong(item)}
            />
          );
        })}
        <ButtonPrimary
          onPress={() => navigation.navigate("Search")}
          title={"ADD"}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0E0B1E",
  },
  wrapper: {
    marginHorizontal: 24,
    marginTop: 30,
  },
  playlistTitle: {
    fontSize: 22,
    fontWeight: "bold",
    color: "white",
  },
});

Home.routeInfo = {
  title: "Home Screen",
  path: "/home",
};

export default Home;
