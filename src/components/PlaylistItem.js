import React from "react";
import { StyleSheet, View, Image, Text, Pressable } from "react-native";

const PlaylistItem = (props) => {
  return (
    <Pressable onPress={props.onPress} style={styles.container}>
      <Image
        source={{ uri: props.thumb }}
        style={{ width: 76, height: 56, borderRadius: 4 }}
      />
      <View style={styles.titleContainer}>
        <Text numberOfLines={2} style={styles.nameSong}>
          {props.name}
        </Text>
        <Text style={styles.nameAuthor}>{props.author}</Text>
      </View>
      <Image
        style={{ width: 24, height: 24, tintColor: "white" }}
        source={require("../assets/sections.png")}
      />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    // paddingHorizontal: 24,
    marginTop: 20,
    alignItems: "center",
  },
  titleContainer: {
    flex: 1,
    marginLeft: 20,
    marginRight: 12,
  },
  nameSong: {
    fontSize: 16,
    color: "white",
  },
  nameAuthor: {
    color: "#ccc",
    fontSize: 12,
    marginTop: 6,
  },
});

export default PlaylistItem;
