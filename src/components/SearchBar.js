import React from "react";
import { StyleSheet, View, TextInput, Image, Text } from "react-native";

const SearchBar = ({ onCancel, onChangeText }) => {
  return (
    <View style={styles.container}>
      <View style={styles.headerSearch}>
        <Image
          style={styles.searchIcon}
          source={require("../assets/search.png")}
        />
        <TextInput
          style={styles.searchInput}
          placeholder="Search"
          onChangeText={onChangeText}
          clearTextOnFocus="true"
        />
        <Image
          style={styles.searchIcon}
          source={require("../assets/cancel.png")}
        />
      </View>
      <Text
        onPress={onCancel}
        style={styles.searchCancel}
      >
        Cancel
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 24,
  },
  headerSearch: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    backgroundColor: "#363942",
    borderRadius: 8,
    height: 40,
  },
  searchIcon: {
    tintColor: "white",
    height: 16,
    width: 16,
    marginHorizontal: 8,
  },
  searchInput: {
    color: "white",
    flex: 1,
    height: "100%",
    outlineWidth: 0,
  },
  searchCancel: {
    paddingLeft: 8,
    color: "#CCFB5F",
  },
});

export default SearchBar;
