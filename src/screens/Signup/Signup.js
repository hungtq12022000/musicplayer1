import React from "react";
import { StyleSheet, View, ImageBackground, Text } from "react-native";
import { InputInfor, ButtonPrimary } from "../../components";
import { useNavFunc } from "../../navigation/useNavFunc";

const backgroundImage = { uri: require("../../assets/backgroundSignup.png") };
const Signup = () => {
  const { navigation } = useNavFunc();
  return (
    <View style={styles.container}>
      <ImageBackground
        source={backgroundImage}
        style={styles.backgroundImage}
        resizeMode="cover"
      >
        <Text style={styles.headerText}>SIGN UP</Text>
        <InputInfor
          title={"Name"}
          linkIcon={require("../../assets/user.png")}
        />
        <InputInfor
          title={"E-Mail"}
          linkIcon={require("../../assets/email.png")}
        />
        <InputInfor
          title={"Password"}
          linkIcon={require("../../assets/lock.png")}
          linkHide={require("../../assets/view.png")}
        />
        <ButtonPrimary
          title={"SIGN UP"}
          onPress={() => navigation.navigate('Signup')}
        />
        <Text
          style={styles.signin}
          onPress={() => navigation.navigate("Signin")}
        >
          SIGN IN
        </Text>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "grey",
  },
  backgroundImage: {
    flex: 1,
    paddingHorizontal: 24,
  },
  headerText: {
    color: "white",
    marginVertical: 100,
    fontSize: 36,
    fontWeight: "bold",
  },
  signin: {
    fontSize: 16,
    fontWeight: "bold",
    color: "white",
    textAlign: "center",
  },
});

Signup.routeInfo = {
  title: "Sign Up",
  path: "/signup",
};

export default Signup;
