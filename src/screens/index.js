export { default as Home } from './Home/Home';
export { default as Search } from './Search/Search';
export { default as Signup } from './Signup/Signup';
export { default as Signin } from './Signin/Signin';
export { default as ForgotPassword } from './ForgotPassword/ForgotPassword';
export { default as ResetPassword } from './ResetPassword/ResetPassword';
export { default as CloneMe } from './_CloneMe';

