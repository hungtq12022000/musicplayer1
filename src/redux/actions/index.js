export const DeviceAction = {
  UPDATE_DEVICE: "UPDATE_DEVICE",
};

export const UserAction = {
  UPDATE_USER_INFO: "UPDATE_USER_INFO",
};

export const YoutubeAction = {
  UPDATE_LIST: 'UPDATE_LIST',
  NEXT_SONG: 'NEXT_SONG',
  PLAY_SONG: 'PLAY_SONG',
};
