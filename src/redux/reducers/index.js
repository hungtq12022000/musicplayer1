import user from './user';
import device from './device';
import youtube from './youtube';

export default {
	user,
	device,
	youtube,
};
