import { YoutubeAction } from "../redux/actions";
import Store from "../redux/Store";
const APIKEY = "AIzaSyCtzbyhs0mnO3647YEASZ3q2AES_LX9sQI";

class YoutubeServices {
  searchByKeyword = async (keyword) => {
    const res = await fetch(
      `https://youtube.googleapis.com/youtube/v3/search?part=snippet&q=${keyword}&key=${APIKEY}`
    );
    const jsonRes = await res.json();
    return jsonRes.items;
  };

  initPlayer = () => {
    const { listMusic, playing } = Store.getState().youtube;
    if (playing) {
    } else {
      if (listMusic && listMusic.length > 0) {
        Store.dispatch({
          type: YoutubeAction.NEXT_SONG,
        });
      }
    }
  };
}
export default new YoutubeServices();
