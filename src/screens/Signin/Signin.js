import React from "react";
import { StyleSheet, View, ImageBackground, Text, Image } from "react-native";
import { InputInfor, ButtonPrimary } from "../../components";
import { useNavFunc } from "../../navigation/useNavFunc";

const backgroundImage = { uri: require("../../assets/backgroundSignin.png") };
const Signin = () => {
  const { navigation } = useNavFunc();
  return (
    <View style={styles.container}>
      <ImageBackground
        source={backgroundImage}
        resizeMode="cover"
        style={styles.backgroundImage}
      >
        <Text style={styles.headerText}>SIGN IN</Text>
        <InputInfor
          title={"E-Mail"}
          linkIcon={require("../../assets/email.png")}
        />
        <InputInfor
          title={"Password"}
          linkIcon={require("../../assets/lock.png")}
          linkHide={require("../../assets/view.png")}
        />
        <Text
          style={styles.forgotPassword}
          onPress={() => navigation.navigate("ForgotPassword")}
        >
          Forgot Password ?
        </Text>
        <ButtonPrimary
          title={"SIGN IN"}
          onPress={() => navigation.navigate("Signin")}
        />
        <View style={styles.connectHeader}>
          <View style={{ flex: 1, height: 1, backgroundColor: "white" }} />
          <Text style={{ color: "white", paddingHorizontal: 8, fontSize: 11 }}>
            Or connect with
          </Text>
          <View style={{ flex: 1, height: 1, backgroundColor: "white" }} />
        </View>
        <View style={styles.connectList}>
          <Image
            style={styles.connectLink}
            source={require("../../assets/facebook.png")}
          />
          <Image
            style={styles.connectLink}
            source={require("../../assets/google-plus.png")}
          />
          <Image
            style={styles.connectLink}
            source={require("../../assets/twitter.png")}
          />
        </View>
        <View style={styles.connectFooter}>
          <Text style={{ color: "white", marginRight: 8 }}>
            Don't have an account ?
          </Text>
          <Text
            style={{ color: "#CBFB5E", fontWeight: "bold" }}
            onPress={() => navigation.navigate("Signup")}
          >
            Sign Up
          </Text>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    paddingHorizontal: 24,
  },
  headerText: {
    color: "white",
    marginVertical: 100,
    fontSize: 36,
    fontWeight: "bold",
  },
  forgotPassword: {
    color: "white",
    textAlign: "right",
    marginBottom: 20,
  },
  connectHeader: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 100,
  },
  connectList: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 24,
  },
  connectLink: {
    backgroundColor: "white",
    borderRadius: "100%",
    width: 40,
    height: 40,
    marginHorizontal: 8,
  },
  connectFooter: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 60,
    marginBottom: 10,
  },
});

Signin.routeInfo = {
  title: "Sign In",
  path: "/signin",
};

export default Signin;
