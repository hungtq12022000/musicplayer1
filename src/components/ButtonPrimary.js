import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";

const ButtonPrimary = (props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button} onPress={props.onPress}>
        <Text style={styles.title}>{props.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    marginVertical: 36,
  },
  button: {
    borderRadius: 5,
    backgroundColor: "#CBFB5E",
    paddingVertical: 12,
    alignItems: "center",
  },
  title: {
    color: "#20242F",
    fontSize: 16,
    fontWeight: "bold",
  },
});

export default ButtonPrimary;
