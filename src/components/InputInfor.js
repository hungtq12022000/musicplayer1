import React from "react";
import { StyleSheet, View, Image, TextInput } from "react-native";

const InputInfor = (props) => {
  return (
    <View style={styles.container}>
      <Image source={props.linkIcon} style={styles.inputInforIcon} />
      <TextInput
        style={styles.inputInforData}
        placeholder={props.title}
        placeholderTextColor={"#9F9F9F"}
      />
      <Image source={props.linkHide} style={styles.inputInforIcon} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 36,
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "white",
    marginBottom: 36,
  },
  inputInforIcon: {
    tintColor: "white",
    height: 16,
    width: 16,
    marginRight: 16,
  },
  inputInforData: {
    flex: 1,
    height: "100%",
    color: "white",
    outlineWidth: 0,
  },
});

export default InputInfor;
