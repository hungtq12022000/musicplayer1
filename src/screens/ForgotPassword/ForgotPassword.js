import React from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableHighlight,
} from "react-native";
import { InputInfor, ButtonPrimary } from "../../components";
import { useNavFunc } from "../../navigation/useNavFunc";

const ForgotPassword = () => {
  const { navigation } = useNavFunc();
  return (
    <View style={styles.container}>
      <TouchableHighlight onPress={() => navigation.navigate("Signin")}>
        <Image
          style={styles.arrowIcon}
          source={require("../../assets/arrowleft.png")}
        />
      </TouchableHighlight>
      <Text style={styles.header}>Forgot Password?</Text>
      <Text style={styles.description}>
        If you need help resetting your password, we can help by sending you a
        link to reset it.
      </Text>
      <InputInfor
        title={"E-Mail"}
        linkIcon={require("../../assets/email.png")}
      />
      <ButtonPrimary
        title={"SENT"}
        onPress={() => navigation.navigate('ResetPassword')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0E0B1F",
    paddingHorizontal: 24,
    paddingTop: 50,
  },
  arrowIcon: {
    tintColor: "white",
    width: 24,
    height: 24,
  },
  header: {
    color: "white",
    fontSize: 28,
    fontWeight: "bold",
    marginVertical: 28,
  },
  description: {
    color: "#8D92A3",
    width: 258,
    lineHeight: 22,
    marginBottom: 90,
  },
});

ForgotPassword.routeInfo = {
  title: "Forgot Password",
  path: "/forgotpassword",
};

export default ForgotPassword;
