import React from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableHighlight,
} from "react-native";
import { InputInfor, ButtonPrimary } from "../../components";

const ResetPassword = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <TouchableHighlight onPress={() => navigation.navigate("ForgotPassword")}>
        <Image
          style={styles.arrowIcon}
          source={require("../../assets/arrowleft.png")}
        />
      </TouchableHighlight>
      <Text style={styles.header}>Forgot Password?</Text>
      <Text style={styles.description}>
        If you need help resetting your password, we can help by sending you a
        link to reset it.
      </Text>
      <InputInfor
        title={"Password"}
        linkIcon={require("../../assets/lock.png")}
        linkHide={require("../../assets/view.png")}
      />
      <InputInfor
        title={"Confirm Password"}
        linkIcon={require("../../assets/lock.png")}
        linkHide={require("../../assets/view.png")}
      />
      <ButtonPrimary title={"CHANGE PASSWORD"} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0E0B1F",
    paddingHorizontal: 24,
    paddingTop: 50,
  },
  arrowIcon: {
    tintColor: "white",
    width: 24,
    height: 24,
  },
  header: {
    color: "white",
    fontSize: 28,
    fontWeight: "bold",
    marginVertical: 28,
  },
  description: {
    color: "#8D92A3",
    width: 258,
    lineHeight: 22,
    marginBottom: 90,
  },
});

ResetPassword.routeInfo = {
  title: "Reset Password",
  path: "/resetpassword",
};

export default ResetPassword;
