import React from "react";
import { Provider } from "react-redux";
import Navigation from "./src/navigation/Navigation";
import Store from "./src/redux/Store";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  ForgotPassword,
  Home,
  ResetPassword,
  Search,
  Signin,
  Signup,
} from "./src/screens";
import "./src/services/FirebaseService";

const Stack = createNativeStackNavigator();

function App() {
  return (
    <Provider store={Store}>
      <Navigation>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Search" component={Search} />
        <Stack.Screen name="Signin" component={Signin} />
        <Stack.Screen name="Signup" component={Signup} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        <Stack.Screen name="ResetPassword" component={ResetPassword} />
      </Navigation>
    </Provider>
  );
}

export default App;
