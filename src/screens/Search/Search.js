import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";
import { useDispatch } from "react-redux";
import { PlaylistItem, SearchBar } from "../../components";
import { useNavFunc } from "../../navigation/useNavFunc";
import FirebaseService from "../../services/FirebaseService";
import YoutubeServices from "../../services/YoutubeServices";

let searchTimeout;

const Search = () => {
  const { navigation } = useNavFunc();
  const [results, setResults] = useState([]);
  const [keyword, setKeyword] = useState("");

  useEffect(() => {
    if (keyword && keyword.length >= 3) {
      if (searchTimeout) clearTimeout(searchTimeout);
      searchTimeout = setTimeout(() => {
        searchByKeyword();
      }, 500);
    }
  }, [keyword]);

  const searchByKeyword = async () => {
    let res = await YoutubeServices.searchByKeyword(keyword);
    setResults(res);
  };

  const onSelectItem = (item) => {
    FirebaseService.addNewSong({
      thumbnail: item.snippet.thumbnails.default.url,
      title: item.snippet.title,
      author: item.snippet.channelTitle,
      videoId: item.id.videoId,
    });
    navigation.navigate('Home')
  };

  return (
    <View style={styles.container}>
      <SearchBar
        navigation={navigation}
        onChangeText={(text) => setKeyword(text)}
        onCancel={() => navigation.navigate("Home")}
      />
      {results.map((item, index) => {
        return (
          <PlaylistItem
            key={`result${index}`}
            thumb={item.snippet.thumbnails.default.url}
            name={item.snippet.title}
            author={item.snippet.channelTitle}
            onPress={() => onSelectItem(item)}
          />
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0E0B1E",
    paddingHorizontal: 24,
  },
});

Search.routeInfo = {
  title: "Search Screen",
  path: "/search",
};

export default Search;
