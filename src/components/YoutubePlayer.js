import React from "react";
import Youtube from "react-youtube";
import { StyleSheet, View, Text } from "react-native";

const opts = {
  width: "100%",
  playerVars: {
    // https://developers.google.com/youtube/player_parameters
    autoplay: 1,
  },
};

const YoutubePlayer = (props) => {
  return (
    <View style={styles.container}>
      <Youtube
        onEnd={props.onEnd}
        onReady={props.onReady}
        videoId={props.videoId}
        opts={opts}
      ></Youtube>
      <View style={styles.titlecontainer}>
        <Text style={styles.nameSong}>{props.nameSong}</Text>
        <Text style={styles.nameAuthor}>{props.nameAuthor}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},

  titlecontainer: {
    paddingHorizontal: 24,
    paddingVertical: 20,
    alignItems: "center",
  },
  nameSong: {
    color: "white",
    fontSize: 18,
  },
  nameAuthor: {
    color: "white",
    fontSize: 12,
    marginTop: 4,
  },
});

export default YoutubePlayer;
