import { YoutubeAction } from "../actions";

const initialState = {
  playing: undefined,
  listMusic: [],
};

const youtube = (state = initialState, action) => {
  switch (action.type) {
    case YoutubeAction.UPDATE_LIST:
      let oldList = [...state.listMusic];
      oldList.push(action.data);
      return {
        ...state,
        listMusic: oldList,
      };
    case YoutubeAction.NEXT_SONG:
      if (!state.listMusic.length) return state;
      let nextSong = state.listMusic[0];
      let newListMusic = [...state.listMusic];
      newListMusic = newListMusic.slice(1);
      return {
        ...state,
        listMusic: newListMusic,
        playing: nextSong,
      };
    case YoutubeAction.PLAY_SONG:
      let newList = state.listMusic.filter(item => item.videoId !== action.data.videoId)
      return {
        ...state,
        listMusic: newList,
        playing: action.data,
      }
    default:
      return state;
  }
};

export default youtube;
